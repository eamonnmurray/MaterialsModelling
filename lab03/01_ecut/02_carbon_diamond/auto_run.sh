#!/bin/bash

template="C_diamond_base.in"
repstr="xxxx"

for val in {10..60..5}
do
  inp="C_diamond_${val}.in"
  sed "s/$repstr/$val/" $template > $inp
  pw.x < $inp &> ${inp%.*}.out
done

awk '/kinetic-energy/{ecut=$4}
     /^!.*total/{print ecut, $5}' *out > etot_v_ecut.dat
